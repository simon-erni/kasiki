package ch.hslu.infsec.kasiki;

import java.util.LinkedList;
import java.util.List;

public class Application {

    public static void main(String[] args) {
        String input = "TFNMJBDCRITVVAFSRCJIFDPINNNMKAPELIWTTPYQFUIWJSJBIXULMGPXRZWNFDQXOPICRSALAERNVVKJHRVKJOJQIMBKBISTZKLZFSMVWPSWXJSLVXJSYIPYFERSWVEVLNFCBHFTDMRXDYTMHIVOIMJIVJZFIMMSFESSRQCQDNFIBISDFUTZUVZWTGZMAFSJQGMOZKLYTFAMHIKMVTCJQIIBQCWYJDUXJFZVQJOJKLRVJAXJEFKLRFYZWJJEIPXFZVIRBJKLNOV";

        char[] inputArray = input.toCharArray();

        for (int i = 0; i < inputArray.length - 2; i++) {
            String searchString = String.valueOf(inputArray, i, 3);

            if (input.indexOf(searchString, i + 1) != -1) {
                System.out.print(searchString + " ");
                List<Integer> results = printAllOccurences(input, searchString, 0, new LinkedList<Integer>());


                for (Integer result : results) {
                    System.out.print(i);
                }

                System.out.println("");
            }
        }
    }

    public static List<Integer> printAllOccurences(String input, String searchString, int index, List<Integer> foundOccurences) {
        int searchResult = input.indexOf(searchString, index);

        if (searchResult != -1) {
            foundOccurences.add(searchResult);
            printAllOccurences(input, searchString, searchResult + 1, foundOccurences);
        }

        return foundOccurences;
    }

}
